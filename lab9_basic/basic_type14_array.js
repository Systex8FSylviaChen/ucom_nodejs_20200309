var a1 = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
var a2 = Array.from('sylvia')
var a3 = 'abcdefg'.split('')
console.log(a1, a2, a3)
console.log(Array.isArray(a1),Array.isArray(a2),Array.isArray(a3))
var a4 = new Array()
a4.push('a')
a4.push('b')
a4.push('c')
a4.push('d')
console.log("a4=",a4, Array.isArray(a4))
// push, pop
console.log("a1=",a1.pop(), a1)//delete element from bottom
a1.push('G')//add element from bottom
console.log("a1=",a1)
console.log("a2=",a2.shift(), a2)//shift right 1
a2.unshift('A')//shift left & add elemtnt from top
console.log("a2=",a2)