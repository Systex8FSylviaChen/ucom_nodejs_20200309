var token = 'abc'
var y = function () {
    var token = 'def'
    console.log('inside', token)
}
console.log('first global', token)
y()//local variable
console.log('second global', token)
var x1 = function (x, y) {
    return x * y
}
var x2 = (x, y) => { return x * y }
var x3 = (x, y) => x * y
console.log(x1(3, 4))
console.log(x2(6, 7))
console.log(x3(8, 9))

var mul = (a, b) => { return a * b }
console.log(mul(10, 11))

var add = (c, d) => c + d
console.log(add(9, 1))