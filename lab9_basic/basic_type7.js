var elements = [0, "", NaN, null, undefined]
console.log("type o f elements:", typeof elements, Array.isArray(elements))

elements.forEach(evaluate)
function evaluate(value, index, array) {
    if (value) {
        console.log(index, value, "ture")
    } else {
        console.log(index, value, "false")
    }
}
console.log(typeof evaluate)

elements.forEach((value, index, array) => {
    if (value) {
        console.log(index, value, "ture")
    } else {
        console.log(index, value, "false")
    }

})