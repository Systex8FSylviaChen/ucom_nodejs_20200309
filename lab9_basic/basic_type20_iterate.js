var x2 = [1, 3, 5, 7, 9, 2, 4, 6, 8]
for (index in x2) {
    console.log("index=", index, "value=", x2[index])
}

for (item of x2) {
    console.log("value=", item)
}

var obj = {
    name: "Sylvia",
    age: 22,
    gender: "female"
}
for (key in obj) {
    console.log(key, "/", obj[key])
}