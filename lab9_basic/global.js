global.coffee = "americano"
global.juice = "avocado blueberry"
global.tea = "breakfast tea"
function printGlobal(arg){
    console.time(global[arg])
    console.log(global[arg])
    // console.warn(global[arg])
    // console.error(global[arg])
    console.timeEnd(global[arg])
}
console.time("TOTAL")
printGlobal("coffee")
printGlobal("juice")
